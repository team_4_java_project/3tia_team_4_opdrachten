/*
	Document   : Student
	Created on : 29-sep-2016, 16:49:48
	Author     : Steven Jonckers
*/


public class Student {
	private String lastName;
	private String firstName;
	private String email;

	public Student() {
	}

	public Student(String lastName, String firstName, String email) {
		this.lastName = lastName;
		this.firstName = firstName;
		this.email = email;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
}
